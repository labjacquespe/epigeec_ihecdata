from __future__ import absolute_import, division, print_function

import argparse
import os

import ihecdata_init

def parse_args(args):

    parser = argparse.ArgumentParser(prog="IHECdata", description="IHECdata - Tool for managing IHEC data cached on Calcul Quebec repositories.")
    subparsers = parser.add_subparsers(help="Sub-command help.")

    parser_init = subparsers.add_parser("init", description="Create the folder structure needed to manage IHEC data.")

    root_default = os.path.join(os.getcwd(), "ihec_data")
    resource_default = os.path.join(os.getcwd(), "resource")

    parser_init.set_defaults(func=ihecdata_init.init)
    parser_init.add_argument("--path", help="The folder where to create the structure. Default='./ihec_data' (TEXT)", default=root_default)

    h_res = "The folder where to seek resource files (chromsizes/filter/metadata). Metadata files must have the format 'ihec_<assembly>_<build>.json'. Default='./resource' (TEXT)."
    parser_init.add_argument("--resource", help=h_res, default=resource_default)

    return parser.parse_args(args)
