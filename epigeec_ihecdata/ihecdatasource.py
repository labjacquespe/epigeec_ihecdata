# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import csv
import glob
import itertools
import json
import os
import os.path
import sys

import pandas as pd

from .ihecdata import IhecData
from . import validators

class IhecDatasource(object):
    def __init__(self, assembly, release):
        self.assembly = assembly
        self.release = release
        self._paths = self._find_files() #dict {basename: path}

    def _find_files(self):
        raise NotImplementedError

    def get_ihecdatas(self, basenames):
        return self._create_ihecdatas(basenames, self.assembly, self.release, self._paths)

    @staticmethod
    def _create_ihecdatas(basenames, assembly, release, paths):
        return [
            IhecData(
                uri=paths.get(basename),
                basename=basename,
                assembly=assembly,
                release=release
            )
            for basename in basenames
        ]


class Mp2bMountIhecDatasource(IhecDatasource):
    DIR = '/nfs3_ib/10.4.217.32/home/genomicdata/ihec_datasets/{release}/*/{assembly}/*'
    SPECIAL_HG38_SHIRAHIGE = '/nfs3_ib/10.4.217.32/home/genomicdata/ihec_datasets/{release}/shirahige/*'

    def _find_files(self):
        """Return {basename:path} dict for all existing bw."""
        mount_dir = self.DIR.format(release=self.release, assembly=self.assembly)
        files = glob.glob(mount_dir)

        if self.assembly == "hg38":
            special_dir = self.SPECIAL_HG38_SHIRAHIGE.format(release=self.release)
            files.extend(glob.glob(special_dir))

        return {os.path.basename(path):path for path in files}

    @staticmethod
    def _path_error_message(basename):
        return "{} is missing from the source files".format(basename)

    def get_paths(self, basenames):
        """Return paths for basenames if they all exist.
        Raise IOError otherwise.
        """
        try:
            return [self._paths[basename] for basename in basenames]
        except KeyError as e:
            raise IOError(self._path_error_message(e))

    def get_path(self, basename):
        """Return path for basename if it exists.
        Raise IOError otherwise.
        """
        try:
            return self._paths[basename]
        except KeyError as e:
            raise IOError(self._path_error_message(e))


class Mp2b2019IhecDatasource(Mp2bMountIhecDatasource):
    DIR = "/nfs3_ib/10.4.219.38/jbodpool/ihec_data/share/{release}/*/{assembly}/*" #infiniband
    # DIR = "/nfs3_ib/10.0.219.38/jbodpool/ihec_data/share/{release}/*/{assembly}/*" #ethernet

    def _find_files(self):
        """Return {basename:path} dict for all existing bw."""
        if self.release != "2019-11":
            raise ValueError("The release is not 2019-11. Datasource only applicable to 2019-11.")

        mount_dir = self.DIR.format(release=self.release, assembly=self.assembly)
        files = glob.glob(mount_dir)
        return {os.path.basename(path):path for path in files}


class LocalIhecDatasource(IhecDatasource):
    """Describe files available for a given assembly/release, and manipulate them"""
    # First level dir/files
    DIFFERENT_DSET_BASENAME = "{assembly}_different_dataset_content.list"
    CHROM_DIR = "chromsizes"
    FILTER_DIR = "filter"
    CHROM_BASENAME = "{assembly}.{name}.chrom.sizes"
    FILTER_BASENAME = "{assembly}.{name}.bed"

    # release/assembly specific files/paths
    DIR = '{release}/{assembly}/'

    IHEC_JSON_BASENAME = "ihec_{assembly}_{release}.json"
    FLAT_JSON_BASENAME = "{assembly}_{release}.json"
    FINAL_JSON_BASENAME = "{assembly}_{release}_final.json"

    MERGE_LIST_BASENAME = "{assembly}_{release}_merge.tsv"
    EXPECTED_MERGES_BASENAME = "{assembly}_{release}_expected_merges.tsv"
    REPORT_BASENAME = "{assembly}_{release}_report.tsv"
    MASTERTABLE_REPORT_BASENAME = "{assembly}_{release}_mastertable_report.tsv"

    MISSING_BASENAME = "{assembly}_{release}_missing.md5"
    BAD_MD5_BASENAME = "{assembly}_{release}_bad_md5.tsv"
    CHROM_ERROR_BASENAME = "{assembly}_{release}_chromsize_error.md5"
    VALID_MERGE_BASENAME = "{assembly}_{release}_valid_merge.md5"
    VALID_STANDARDIZE_BASENAME = "{assembly}_{release}_valid_standardize.md5"

    BIGWIG_DIR = "bw"
    MERGES_DIR = "mn"
    BEDGRAPH_DIR = "bg"
    HDF5_DIR = "hdf5"

    def __init__(self, assembly, release, datapath):
        self.datapath = os.path.abspath(datapath)

        self._partial_name = {"assembly":assembly, "release":release}

        self.local_dir = os.path.join(
            self.datapath,
            self.DIR.format(release=release, assembly=assembly)
        )

        # Shortened names for practicality
        self._reader = LocalIhecDatasourceReader()
        self._writer = LocalIhecDatasourceWriter()
        self._updater = ValidUpdater()

        super(LocalIhecDatasource, self).__init__(assembly, release) # Appel le constructeur de IhecDatasource

    # Internal/private methods

    def _find_files(self):
        """Return basename:path dict for all release/assembly level paths."""
        files = glob.glob(os.path.join(self.local_dir, '*'))
        return {os.path.basename(path):path for path in files}

    def _get_path(self, basename):
        """Return the path of basename if it exists on the first release/assembly level.
        Raise IOError otherwise.
        """
        try:
            return self._paths[basename]
        except KeyError:
            s = "'{basename}' does not exist in the local subfolder:\n'{dir}'.".format(
                basename=basename, dir=self.local_dir)
            raise IOError(s)

    def _get_general_file(self, folder, basename_template, name):
        """Return path of a general file (chrom/filter) with given name,
        if it exists. Raise IOError otherwise.
        """
        folder_path = os.path.join(self.datapath, folder, '*')
        files = {os.path.basename(path):path for path in glob.glob(folder_path)}
        basename = basename_template.format(
            assembly=self.assembly,
            name=name
        )
        try:
            return files[basename]
        except KeyError:
            s = "'{basename}' does not exist in the general folder:\n'{dir}'.".format(
                basename=basename, dir=folder_path)
            raise IOError(s)

    def _complete_name(self, template):
        """Add partial name to basename template."""
        return template.format(**self._partial_name)

    def _create_path(self, template):
        """Create path from basename template, usually to write a file."""
        basename = self._complete_name(template)
        return os.path.join(self.local_dir, basename)

    def _read(self, template, read_method):
        """Read file associated to template if it exists.
        Raise IOError otherwise.
        """
        basename = self._complete_name(template)
        path = self._get_path(basename)
        return read_method(path)

    def _write(self, template, write_method, content):
        """Write content to file associated to template."""
        path = self._create_path(template)
        write_method(path, content)

    # Public read/write methods for specific files.
    # Raise IOError when trying to read non existing file

    def read_different_dsets(self):
        """Return the set of dset which have different content between releases."""
        basename = self.DIFFERENT_DSET_BASENAME.format(assembly=self.assembly)
        path = os.path.join(self.datapath, basename)
        return self._reader.list_file(path)

    def read_ihec_json(self):
        """Return the IHEC json if it exists."""
        return self._read(self.IHEC_JSON_BASENAME, self._reader.json)

    def read_flat_json(self):
        """Return the flat json if it exists. """
        return self._read(self.FLAT_JSON_BASENAME, self._reader.epigeec_json)

    def write_flat_json(self, content):
        """Write flat json to file."""
        self._write(self.FLAT_JSON_BASENAME, self._writer.epigeec_json, content)

    def read_final_json(self):
        """Return the final json if it exists."""
        return self._read(self.FINAL_JSON_BASENAME, self._reader.epigeec_json)

    def write_final_json(self, content):
        """Write final json to file."""
        self._write(self.FINAL_JSON_BASENAME, self._writer.epigeec_json, content)

    def read_merge_info(self):
        """Read merge file into a returned dataset:md5s dict."""
        return self._read(self.MERGE_LIST_BASENAME, self._reader.to_merge)

    def write_merge_info(self, content):
        """Write 'dataset_name md5_1 ... md5_n' lines to merge file."""
        self._write(self.MERGE_LIST_BASENAME, self._writer.to_merge, content)

    def read_report(self):
        """Read report and return it as pandas DataFrame."""
        return self._read(self.REPORT_BASENAME, self._reader.report)

    def write_report(self, content):
        """Write report to file."""
        self._write(self.REPORT_BASENAME, self._writer.report, content)

    def write_mastertable_report(self, report_df):
        """Write masterTable report to file."""
        self._write(self.MASTERTABLE_REPORT_BASENAME, self._writer.dataframe, report_df)

    def read_expected_merges(self):
        """Read expected merges files into a returned (dset,merge_md5) list."""
        return self._read(self.EXPECTED_MERGES_BASENAME, self._reader.expected_merges)

    def write_expected_merges(self, pairs):
        """Write 'dset new_md5' pairs to file."""
        self._write(self.EXPECTED_MERGES_BASENAME, self._writer.expected_merges, pairs)

    def read_valid_merge(self):
        """Read the valid merge file into a returned md5 set."""
        return self._read(self.VALID_MERGE_BASENAME, self._reader.list_file)

    def write_valid_merge(self, md5s):
        """Write md5s to valid merge file."""
        self._write(self.VALID_MERGE_BASENAME, self._writer.list_file, md5s)

    def update_valid_merge(self):
        """Update valid merge file and return valid md5s set."""
        already_treated_bws = self.md5_path_dict(self.current_mergenames)

        valid_md5s = self._updater.update_md5_file(
            read_method=self.read_valid_merge,
            write_method=self.write_valid_merge,
            treated_files=already_treated_bws,
            file_type="merge"
        )
        return valid_md5s

    def read_missing_md5s(self):
        """Read the missing files fileinto a returned md5 set."""
        return self._read(self.MISSING_BASENAME, self._reader.list_file)

    def write_missing_md5s(self, md5s):
        """Write md5s to missing md5s file."""
        self._write(self.MISSING_BASENAME, self._writer.list_file, md5s)

    def read_bad_md5(self):
        """Read the bad md5 file into a returned [basename, computed_md5] pairs list."""
        return self._read(self.BAD_MD5_BASENAME, self._reader.md5_pairs)

    def write_bad_md5(self, pairs):
        """Write bad md5s pairs file."""
        self._write(self.BAD_MD5_BASENAME, self._writer.md5_pairs, pairs)

    def read_chromsize_error_md5s(self):
        """Read the chromsize error file into a returned md5 set."""
        return self._read(self.CHROM_ERROR_BASENAME, self._reader.list_file)

    def read_valid_standardize(self):
        """Read the valid standardize file into a returned md5 set."""
        return self._read(self.VALID_STANDARDIZE_BASENAME, self._reader.list_file)

    def write_valid_standardize(self, md5s):
        """Write valid standardize md5s file."""
        self._write(self.VALID_STANDARDIZE_BASENAME, self._writer.list_file, md5s)

    def update_valid_standardize(self):
        """Update valid standardize file and return valid md5s set."""
        already_treated_bws = self.md5_path_dict(self.current_bigwigs)

        valid_md5s = self._updater.update_md5_file(
            read_method=self.read_valid_standardize,
            write_method=self.write_valid_standardize,
            treated_files=already_treated_bws,
            file_type="standardize"
        )
        return valid_md5s

    # Public general directory structure description

    def get_chromsize(self, name):
        """Return path of chromsize file with name."""
        return self._get_general_file(self.CHROM_DIR, self.CHROM_BASENAME, name)

    def get_filter(self, name):
        """Return path of filter file with name."""
        if name == "all" or name == "none":
            return ''
        return self._get_general_file(self.FILTER_DIR, self.FILTER_BASENAME, name)

    @property
    def bigwig_dir(self):
        """Return the path to the bigwig directory."""
        return self._get_path(self.BIGWIG_DIR)

    @property
    def current_bigwigs(self):
        """Return a list of all paths in the bigwig directory."""
        return glob.glob(os.path.join(self.bigwig_dir, '*'))

    @property
    def current_merged_bigwigs(self):
        """Return a list of all paths in the bigwig directory with a ".merge"."""
        return glob.glob(os.path.join(self.bigwig_dir, '*.merge'))

    @property
    def current_bigwigs_alt(self):
        """Return a set of existing bigwigs paths, excluding .merge paths"""
        return set(self.path_without_merge(path) for path in self.current_bigwigs)

    @property
    def current_bigwig_md5s(self):
        """Return a set of the current bigwig md5s."""
        return set(self.paths_to_md5s(self.current_bigwigs))

    def assert_current_bigwigs(self):
        """Raise AssertionError if the number of valid md5s (from file)
        is not same as the number of valid bigWigs (from directory)
        """
        nb_valid = len(self.read_valid_standardize())
        nb_bw = len(self.current_bigwig_md5s)

        if nb_valid != nb_bw:
            message = "{} valid standardize md5s and {} bigWigs present.".format(nb_valid, nb_bw)
            raise AssertionError(message)

    @property
    def mergename_dir(self):
        """Return the path to the merged bigwig directory."""
        return self._get_path(self.MERGES_DIR)

    @property
    def current_mergenames(self):
        """Return a list of all paths in the merged bigwig directory."""
        return glob.glob(os.path.join(self.mergename_dir, '*'))

    @property
    def current_mergename_md5s(self):
        """Return a set of the current merged bigwig md5s in the merge folder."""
        return set(self.paths_to_md5s(self.current_mergenames))

    @property
    def bedgraph_dir(self):
        """Return the path to the bedgraph directory."""
        return self._get_path(self.BEDGRAPH_DIR)

    @property
    def current_bedgraphs(self):
        """Return a list of all paths in the bedgraph directory."""
        return glob.glob(os.path.join(self.bedgraph_dir, '*'))

    @property
    def current_bedgraph_md5s(self):
        """Return a set of the current bedgraph md5s."""
        return set(self.paths_to_md5s(self.current_bedgraphs))

    @property
    def hdf5_dir(self):
        """Return the path to the hdf5 directory."""
        return self._get_path(self.HDF5_DIR)

    def current_hdf5_paths(self):
        """Return a list of all (md5, hdf5 filepath)."""
        return Hdf5Dir(self.hdf5_dir).get_all_hdf5_paths()

    def detail_current_hdf5s(self):
        """Print information about existing hdf5s"""
        nb_bw = len(self.read_valid_standardize())
        print("{} valid standardize bigWig".format(nb_bw))
        Hdf5Dir(self.hdf5_dir).print_hdf5_info()

    def find_all_missing_hdf5s(self, verbose=False):
        """Return list of md5s present in valid standardize file
        but not in at least one hdf5 directory.
        """
        self.assert_current_bigwigs()
        hdf5_dir = Hdf5Dir(self.hdf5_dir)
        return hdf5_dir.find_all_missing_md5s(self.current_bigwig_md5s, verbose)

    def find_specific_missing_hdf5s(self, hdf5_directory, metric, verbose=False):
        """Return list of md5s present in valid standardize file
        but not in a specific hdf5 directory.
        """
        self.assert_current_bigwigs()
        hdf5_dir = Hdf5Dir(self.hdf5_dir)
        return hdf5_dir.find_specific_missing_md5s(self.current_bigwig_md5s, hdf5_directory, metric, verbose)

    # Public path/md5s handling

    @staticmethod
    def path_without_merge(path):
        """Return path with no extension."""
        folder, basename = os.path.split(path)
        basename = basename.split('.')[0]
        return os.path.join(folder, basename)

    @staticmethod
    def path_to_md5(path):
        """Return basename with no extension."""
        return os.path.basename(path).split('.')[0]

    def paths_to_md5s(self, paths):
        """Return basenames with no extension."""
        return [self.path_to_md5(path) for path in paths]

    def md5_path_dict(self, paths):
        """Return basename:path dict for given paths."""
        return {self.path_to_md5(path):path for path in paths}

    # Create paths for new files

    def make_bedgraph_paths(self, basenames):
        """Create and return bedgraph paths for given basenames."""
        return [os.path.join(self.bedgraph_dir, basename) for basename in basenames]

    def make_bigwig_paths(self, basenames):
        """Create and return bigwig paths for given basenames."""
        return [os.path.join(self.bigwig_dir, basename) for basename in basenames]

    def make_merge_path(self, md5):
        """Create and return merged bigwig path for given md5."""
        return os.path.join(self.mergename_dir, "{}.merge".format(md5))


class LocalIhecDatasourceReader(object):
    """Define how to read specific file formats."""

    @staticmethod
    def json(path):
        """Load and return a given json."""
        with open(path, 'r') as f:
            return json.load(f)

    @staticmethod
    def epigeec_json(path):
        """Load and return datasets from epigeec json."""
        with open(path, 'r') as f:
            return json.load(f)["datasets"]

    @staticmethod
    def list_file(path):
        """Load and return list file (one element per line) content."""
        with open(path, 'r') as f:
            return set(line.strip() for line in f)

    @staticmethod
    def to_merge(path):
        """Load and return merge list file content."""
        with open(path, 'r') as f:
            reader = csv.reader(f, delimiter='\t')
            return {row[0]:row[1:] for row in reader}

    @staticmethod
    def report(path):
        """Load and return report content as DataFrame."""
        return pd.read_csv(path, sep='\t')

    def md5_pairs(self, path):
        """Load and return md5 pairs file content."""
        with open(path, 'r') as f:
            reader = csv.reader(f, delimiter='\t')
            next(reader) #skip header
            return self._read_pairs(reader)

    def expected_merges(self, path):
        """Load and return expected merges file content."""
        with open(path, 'r') as f:
            reader = csv.reader(f, delimiter='\t')
            return self._read_pairs(reader)

    @staticmethod
    def _read_pairs(reader):
        return [(row[0], row[1]) for row in reader]


class LocalIhecDatasourceWriter(object):
    """Define how to write specific file formats."""

    @staticmethod
    def json(path, content):
        """Write content to json path"""
        with open(path, 'w') as f:
            json.dump(content, f)

    @staticmethod
    def epigeec_json(path, datasets):
        """Write content to json path"""
        with open(path, 'w') as f:
            json.dump({"datasets":datasets}, f)

    @staticmethod
    def list_file(path, elements):
        """Write elements to path, one per line."""
        with open(path, 'w') as f:
            for elem in elements:
                f.write("{}\n".format(elem))

    @staticmethod
    def to_merge(path, content):
        """Write 'dset md5_1 ... md5_n' lines to path."""
        with open(path, 'w') as f:
            for row in content:
                f.write('\t'.join(row) + '\n')

    @staticmethod
    def report(path, content):
        """Write report content to file."""
        df = pd.DataFrame.from_records(content)
        df.to_csv(path, index=False, sep='\t')

    @staticmethod
    def dataframe(path, df):
        """Write dataframe content to file."""
        df.to_csv(path, index=False, sep='\t')

    def md5_pairs(self, path, pairs):
        """Write md5 pairs to path."""
        with open(path, 'w') as f:
            f.write("filename\tcomputed_md5\n")
            self._write_pairs(f, pairs)

    def expected_merges(self, path, pairs):
        """Write 'dset new_md5' pairs to path."""
        with open(path, 'w') as f:
            self._write_pairs(f, pairs)

    @staticmethod
    def _write_pairs(file, pairs):
        for pair in pairs:
            file.write("{}\t{}\n".format(*pair))


class ValidUpdater(object):
    """Define how to update specific file formats."""

    @staticmethod
    def _get_valid_md5s(read_method, file_type):
        """Return valid md5s from read file. The
        set is empty if the file does not exist.
        """
        try:
            md5s = set(read_method())
        except IOError:
            print("Cannot find valid {} md5 file. Creating it".format(file_type))
            md5s = set([])

        return md5s

    @staticmethod
    def _return_new_valid_md5s(treated_bws, valid_md5s):
        """Return valid md5s from new treated files.
        treated_bws needs to be a basename:path dict.
        """
        bws_to_verify = {md5:path for md5, path in treated_bws.items()
                         if md5 not in valid_md5s}

        new_valid_md5s = set(md5 for md5, path in bws_to_verify.items()
                             if validators.is_bw(path))

        return new_valid_md5s

    def update_md5_file(self, read_method, write_method, treated_files, file_type):
        """Update md5 file through its read/write method. Return all valid md5s.
        treated_files needs to be a basename:path dict.
        """
        valid_md5s = self._get_valid_md5s(read_method, file_type)

        new_valid_md5s = self._return_new_valid_md5s(treated_files, valid_md5s)

        all_valid_md5s = valid_md5s | new_valid_md5s

        if all_valid_md5s != valid_md5s:
            write_method(all_valid_md5s)
        elif not all_valid_md5s:
            write_method(all_valid_md5s)

        return all_valid_md5s


class Hdf5Dir(object):
    """Represent the state of the 'hdf5' directory (with lists/matrices + hdf5s)."""
    DIR = '{resolution}_{select}_{exclude}/{md5}_{resolution}_{select}_{exclude}_{metric_hdf5}.hdf5'
    METRIC_HDF5 = {
        "pearson": "value",
        "spearman": "rank"
    }

    def __init__(self, path):
        self.base_dir = path
        self.hdf5_md5s = self._get_all_hdf5_md5s() # {dirname:{metric:set_of_md5s}}

    @staticmethod
    def _get_md5sum_and_metric(hdf5_filename):
        """Return md5sum and metric for the given hdf5 basename."""
        basename = os.path.basename(hdf5_filename)
        split_basename = os.path.splitext(basename)[0].split('_')
        return [split_basename[0], split_basename[-1]]

    @staticmethod
    def _get_hdf5_dirs(path):
        """Return dirname list for directories present at given path."""
        return next(os.walk(path))[1]

    def _get_dir_files(self, dirname):
        """Return the list of files in dirname."""
        dir_path = os.path.join(self.base_dir, dirname, '*')
        return glob.glob(dir_path)

    def _get_all_hdf5_md5s(self):
        """Return {dirname:{metric:set_of_md5s}} dict."""
        current_md5s = {}

        # sort (md5, metric) list by metric
        sort_key = lambda x: x[1]

        for dirname in self._get_hdf5_dirs(self.base_dir):

            # the sorting is necessary for grouping step
            metric_md5s = sorted(
                (self._get_md5sum_and_metric(file)
                 for file in self._get_dir_files(dirname)),
                key=sort_key
                )

            grouped_metric_md5s = itertools.groupby(metric_md5s, key=sort_key)

            current_md5s[dirname] = {
                metric:set(md5 for md5, _ in grouper)
                for metric, grouper in grouped_metric_md5s
                }

        return current_md5s

    def print_hdf5_info(self):
        """Print the number of unique md5s/files in each directory, for each metric."""
        print("{} hdf5 directories.".format(len(self.hdf5_md5s)))
        for dirname, metric_md5s in self.hdf5_md5s.items():
            print(dirname)
            for metric, md5s in metric_md5s.items():
                print(metric, len(md5s))

    def find_all_missing_md5s(self, wanted_md5s, verbose=False):
        """Return wanted md5s not already present in at least one hdf5 directory."""
        wanted = set(wanted_md5s)
        missing_md5s = []

        for dirname, metric_md5s in self.hdf5_md5s.items():
            for metric, current_md5s in metric_md5s.items():

                missing = wanted - current_md5s
                missing_md5s.extend(missing)

                if missing and verbose:
                    print("{nb} {metric} files missing from {dirname}.".format(
                        nb=len(missing), metric=metric, dirname=dirname))

        return missing_md5s

    def find_specific_missing_md5s(self, wanted_md5s, hdf5_directory, metric, verbose=False):
        """Return wanted md5s not already present in a specific directory"""
        wanted = set(wanted_md5s)
        try:
            present_md5s = self.hdf5_md5s[hdf5_directory][self.METRIC_HDF5[metric]]
        except KeyError as e:
            print("Bad directory or metric name :\n", e)
            print("Present directory names for this assembly are :\n", self.hdf5_md5s.keys())
            sys.exit()

        missing = list(wanted - present_md5s)

        if missing and verbose:
            print("{nb} {metric} files missing from {dirname}.".format(
                nb=len(missing), metric=metric, dirname=hdf5_directory))

        return missing

    def get_all_hdf5_paths(self):
        """Return a list (md5, hdf5 path)."""
        template = "{dirname}/{md5}_{dirname}_{metric_hdf5}.hdf5"
        pairs = []
        for dirname, metric_md5s in self.hdf5_md5s.items():
            for metric, current_md5s in metric_md5s.items():
                for md5 in current_md5s:
                    path = template.format(
                        dirname=dirname,
                        md5=md5,
                        metric_hdf5=metric
                        )
                    path = os.path.join(self.base_dir, path)
                    pairs.append((md5, path))
        return pairs
