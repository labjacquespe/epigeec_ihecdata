from __future__ import absolute_import, division, print_function

import sys

import input_parser

def main(argv):
    args = input_parser.parse_args(argv)
    args.func(args)

def cli():
    main(sys.argv[1:])

if __name__ == "__main__":
    cli()
