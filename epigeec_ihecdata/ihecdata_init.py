from __future__ import absolute_import, division, print_function

from pwd import getpwuid
import os
import os.path
import shutil
import subprocess
import sys

import validators

RELEASES = ["2016-11", "2017-10", "2018-10"]
ASSEMBLIES = ["hg19", "hg38", "mm10"]
GENERAL_FOLDER_NAMES = ["chromsizes", "filter"]
SUBFOLDER_NAMES = ["bw", "bg", "mn", "hdf5"]
DIR_PERMISSION = 775
FILE_PERMISSION = 664
GROUP = "def-jacquesp"

def init(args):
    """Create local folder structure.
    Raise errors if folders are not valid."""
    root_folder = os.path.abspath(args.path)
    resource_folder = os.path.abspath(args.resource)

    print("Checking folders")
    validators.valid_write_folder(root_folder)
    validators.valid_resource_folder(resource_folder)

    _add_folders(resource_folder, root_folder)

    _correct_permissions(root_folder, DIR_PERMISSION, FILE_PERMISSION, GROUP)

def _add_folders(src, dst):
    """Write folders and src resource files to dst."""
    print("Copying filter and chromsizes.")
    for name in GENERAL_FOLDER_NAMES:
        src_folder = os.path.join(src, name)
        dst_folder = os.path.join(dst, name)
        shutil.copytree(src_folder, dst_folder)

    print("Writing folders and copying metadata.")
    for release in RELEASES:
        for assembly in ASSEMBLIES:
            meta_src = os.path.join(src, "metadata/ihec_{}_{}.json".format(assembly, release))
            meta_dst = os.path.join(dst, "{}/{}/".format(release, assembly))
            os.makedirs(meta_dst)
            shutil.copy(meta_src, meta_dst)
            for name in SUBFOLDER_NAMES:
                subfolder = os.path.join(meta_dst, name)
                os.makedirs(subfolder)

def _correct_permissions(path, dir_permission, file_permission, group):
    """Recursively change the permissions of
    path with bash chown and chmod."""
    user = _find_owner(path)
    chown = "chown -R {}:{} {}".format(user, group, path)

    chmod_dir = "find {} -type d -exec chmod {} -- {{}} +".format(path, dir_permission)
    chmod_file = "find {} -type f -exec chmod {} -- {{}} +".format(path, file_permission)

    print("Running commands to change permissions:")
    print(chown)
    _run_command(chown)
    print(chmod_dir)
    _run_command(chmod_dir)
    print(chmod_file)
    _run_command(chmod_file)

def _find_owner(filename):
    """Return user name of file owner."""
    return getpwuid(os.stat(filename).st_uid).pw_name

def _run_command(unsplit_command):
    """Run a command from a string, print
    occuring errors info to stderr."""
    code = subprocess.call(unsplit_command.split())
    if code != 0:
        print("Error code {}: {}".format(code, unsplit_command), file=sys.stderr)
