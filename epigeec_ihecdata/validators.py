from __future__ import absolute_import, division, print_function

import glob
import itertools
import os
import struct

def _valid_folder(path):
    """Raise IOError if path is
    not an existing directory."""
    if not os.path.exists(path):
        raise IOError("{} doesn't exist.".format(path))
    elif not os.path.isdir(path):
        raise IOError("{} is not a directory.".format(path))

def valid_write_folder(path):
    """Raise IOError if program has
    no write permission to path."""
    _valid_folder(path)
    if not os.access(path, os.W_OK):
        raise IOError("{}: Writing permission denied.".format(path))

def valid_read_folder(path):
    """Raise IOError if program has
    no read permission to path."""
    _valid_folder(path)
    if not os.access(path, os.R_OK):
        raise IOError("{}: Reading permission denied.".format(path))

def valid_resource_folder(path):
    """Raise IOError if the path
    is not a correct resource folder."""
    RESOURCE_FOLDERS = ["filter", "metadata", "chromsizes"]
    valid_read_folder(path)

    subpaths = os.path.join(path, "./*")
    found_dir = set(os.path.basename(subpath) for subpath in glob.glob(subpaths))
    for folder in RESOURCE_FOLDERS:
        if folder not in found_dir:
            raise IOError("Could not find {} resource folder.".format(folder))

def is_bg(path):
    with open(path) as bg:
        max_lines = 10 #number of lines to validate
        for line in itertools.islice(bg, max_lines):
            line = bg.next().strip()
            if line:
                line = line.split()
                if not len(line) >= 4: return False
                if not is_int(line[1]): return False
                if not is_int(line[2]): return False
                if not is_float(line[3]): return False
    return True

def is_bw(path):
    bw_magic = 0x888FFC26
    with open(path, "rb") as bw:
        magic = struct.unpack("I", bw.read(4))[0]
        if magic != bw_magic:
            return False
    return True

def is_int(i):
    try: int(i)
    except ValueError:
        return False
    return True

def is_float(f):
    try: float(f)
    except ValueError:
        return False
    return True

def valid_bws(bw_paths):
    return [bw for bw in bw_paths if is_bw(bw)]
