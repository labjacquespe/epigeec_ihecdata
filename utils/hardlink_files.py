from __future__ import absolute_import, print_function

import os
import os.path
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from epigeec_ihecdata import ihecdatasource
from json_info import get_all_expected_final_md5s


def verif_assembly(assembly):
    """Raise ValueError if assembly string is not valid."""
    if assembly not in frozenset(["hg19", "hg38", "mm10"]):
        raise ValueError("{} not an accepted assembly".format(assembly))


def check_same_assembly(datasource1, datasource2):
    """Raise ValueError if the datasources don't have the same assembly."""
    if datasource1.assembly != datasource2.assembly:
        raise ValueError("The datasources assemblies need to be the same.")


def try_link(src, dst):
    """Try to hardlink src to dst.
    Return True if successful.
    Return false and print error otherwise.
    """
    try:
        os.link(src, dst)
        return True
    except OSError as err:
        print(err, dst)
        return False


def try_unlink(file):
    """Try to unlink/delete file.
    Return True if successful.
    Return false and print error otherwise.
    """
    try:
        os.unlink(file)
        return True
    except OSError as err:
        print(err)
        return False


def find_md5_to_hardlink_1(datasource):
    """Return set of md5s to potentially hardlink,
    according to changed_folders and final metadata json.
    """
    # (assembly, releasing_group) pairs
    changed_folders = frozenset([
        ("hg19", "ENCODE"), ("hg38", "ENCODE"), ("mm10", "ENCODE"), ("hg38", "CEMT")
        ])

    return set([
        dset["md5sum"] for dset in datasource.read_final_json()
        if (dset["assembly"], dset["releasing_group"]) not in changed_folders
    ])


def find_md5_to_hardlink_2(old_datasource, new_datasource):
    """Return set of md5s to needed in new datasource and present in old datasource

    UNTESTED
    """
    needed_md5s = get_all_expected_final_md5s(new_datasource)
    already_done_md5s = old_datasource.read_valid_standardize()
    return needed_md5s & already_done_md5s

def hardlink_bigwigs(md5s_to_link, src_datasource, dst_datasource):
    """Hardlink bigwigs from src to dst."""
    check_same_assembly(src_datasource, dst_datasource)

    # find source paths
    all_src_bigwig_paths = src_datasource.md5_path_dict(src_datasource.current_bigwigs_alt)
    src_hardlink_paths = [
        all_src_bigwig_paths[md5] for md5 in md5s_to_link
    ]
    # make destination paths
    dst_hardlink_paths = dst_datasource.make_bigwig_paths(md5s_to_link)

    # hardlink
    i = 0
    for src, dst in zip(src_hardlink_paths, dst_hardlink_paths):
        # os.unlink(dst)
        if not try_link(src, dst):
            i += 1
    print("{} bigwigs could not be linked because of an error.".format(i))


def unlink_bigwigs(md5s, datasource):
    """Unlink/delete bigwigs with given md5s in local source"""
    # take care of all non ".merge files"
    paths = datasource.make_bigwig_paths(md5s)
    fail_counter = 0
    for path in paths:
        if not try_unlink(path):
            fail_counter += 1
    print("{} bigwigs could not be unlinked because of an error.".format(fail_counter))

    # take care of ".merge" files
    fail_counter = 0
    absent_counter = 0
    merge_dict = datasource.md5_path_dict(datasource.current_merged_bigwigs)
    for md5 in md5s:
        if md5 in merge_dict:
            if not try_unlink(merge_dict[md5]):
                fail_counter += 1
        else :
            absent_counter += 1
    print("{} '.merge' bigwigs could not be unlinked because of an error.".format(fail_counter))
    print("{} expected '.merge' bigwigs could not be found".format(absent_counter))


def hardlink_hdf5s(md5s_to_link, src_datasource, dst_datasource):
    """Hardlink hdf5s from src to dst."""
    check_same_assembly(src_datasource, dst_datasource)

    # find source paths
    pairs = src_datasource.current_hdf5_paths()

    # make destination paths and hardlink
    i = 0
    for md5, src_path in pairs:
        if md5 in md5s_to_link:
            dst_path = src_path.replace(src_datasource.release, dst_datasource.release)
            # os.unlink(dst_path)
            if not try_link(src_path, dst_path):
                i += 1
    print("{} hdf5s could not be linked because of an error.".format(i))


def unlink_hdf5s(md5s_to_unlink, datasource):
    """Unlink/delete hdf5s with given md5s in local source"""
    # find source paths
    for md5, path in datasource.current_hdf5_paths():
        if md5 in md5s_to_unlink:
            os.unlink(path)


def update_validation_files(datasource):
    """Update valid_merge.md5 and valid_standardize.md5 files in datasource.

    This needs to be done differently here because the normal merge validation
    process is skipped.
    """
    print("valid standardize before: {}".format(len(datasource.read_valid_standardize())))
    valid_final_md5s = datasource.update_valid_standardize()
    print("valid standardize after: {}".format(len(valid_final_md5s)))

    valid_merge_md5s = datasource.read_valid_merge()
    expected_merge_md5s = set(md5 for _, md5 in datasource.read_expected_merges())

    new_valid_merge_md5s = valid_merge_md5s | (valid_final_md5s & expected_merge_md5s)
    print("valid merge before: {}".format(len(valid_merge_md5s)))
    print("valid merge after: {}".format(len(new_valid_merge_md5s)))

    datasource.write_valid_merge(new_valid_merge_md5s)


def remove_unwanted_files(datasource):
    """Unlink bigwigs and hdf5s that shouldn't be in the datasource.

    Update the validation files too.
    """
    needed_md5s = get_all_expected_final_md5s(datasource)

    present_md5s = datasource.read_valid_standardize()

    unwanted_md5s = present_md5s - needed_md5s

    unlink_bigwigs(unwanted_md5s, datasource)
    unlink_hdf5s(unwanted_md5s, datasource)

    datasource.write_valid_standardize(present_md5s - unwanted_md5s)
    merge_md5s = datasource.read_valid_merge()
    datasource.write_valid_merge(merge_md5s - unwanted_md5s)

    update_validation_files(datasource)


def main(argv):
    """python code.py assembly"""
    assembly = argv[0]
    datapath = "/nfs3_ib/ip29/ip29/jacques_group/local_ihec_data"

    verif_assembly(assembly)

    local_src_2018 = ihecdatasource.LocalIhecDatasource(assembly, "2018-10", datapath)
    local_src_2019 = ihecdatasource.LocalIhecDatasource(assembly, "2019-11", datapath)

    remove_unwanted_files(local_src_2019)

    # to_hardlink_md5s = find_md5_to_hardlink_1(local_src_2018)
    # print("{} md5sum to hardlink.".format(len(to_hardlink_md5s)))

    # hardlink_bigwigs(
    #     md5s_to_link=to_hardlink_md5s,
    #     src_datasource=local_src_2018,
    #     dst_datasource=local_src_2019
    #     )

    # hardlink_hdf5s(
    #     md5s_to_link=to_hardlink_md5s,
    #     src_datasource=local_src_2018,
    #     dst_datasource=local_src_2019
    #     )

    # update_validation_files(local_src_2019)

if __name__ == "__main__":
    main(sys.argv[1:])
