"""
Convert matrix labels using various metadata.
"""
from __future__ import absolute_import, division, print_function

import argparse
import json
import os.path
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from epigeec_ihecdata.ihecdatasource import LocalIhecDatasource
from json_info import get_merge_md5_dict


def galaxy_source_md5_dict(datasets):
    """Return galaxy_label:md5sum dict from dataset metatada list."""
    return {dset["file_name"]:dset["md5sum"] for dset in datasets}


def md5sum_to_file_name_dict(datasets):
    """Return md5sum:file_name dict from dataset metatada list."""
    return {dset["md5sum"]:dset["file_name"] for dset in datasets}


def merge_md5_dict(assembly, release, datapath):
    """Return source_md5:merge_md5 dict from local metadata."""
    local_src = LocalIhecDatasource(assembly, release, datapath)
    return get_merge_md5_dict(local_src)


def convert_matrix(matrix_path, output_path, new_labels):
    """Write converted matrix with new labels to output_path."""
    with open(matrix_path, 'r') as in_mat, open(output_path, 'w') as out_mat:

        new_line = change_header(in_mat.readline(), new_labels)
        out_mat.write(new_line)

        for line in in_mat:
            new_line = change_line(line, new_labels)
            out_mat.write(new_line)


def change_header(line, new_labels):
    """Return first line with changed labels."""
    new_line = [new_labels.get(label, label) for label in line.strip().split('\t')]
    return '\t'.join(new_line) + '\n'


def change_line(line, new_labels):
    """Return non-header line with changed label"""
    label, rest_of_line = line.split('\t', 1)
    new_label = new_labels.get(label, label)
    return '\t'.join([new_label, rest_of_line])


def get_datasets_content(json_path):
    """Return datasets from metadata."""
    return json.load(open(json_path, 'r'))["datasets"]


def parse_args(argv):
    """Define and return argument parser."""
    parser = argparse.ArgumentParser()
    parser.add_argument("matrix", help="Input matrix")
    parser.add_argument("output", help="Converted matrix")
    return parser.parse_args(argv)


def convert_file_name(in_mat, out_mat, assembly):
    """Conversion from (galaxy) "file_name" to "md5sum"."""
    metadata = "/project/6007017/rabyj/galaxy_matrix_all_2017-10/epiGeEC_datasets_{}_2017-10.json".format(assembly)

    file_name_to_md5sum = galaxy_source_md5_dict(
        datasets=get_datasets_content(metadata)
    )

    convert_matrix(in_mat, out_mat, file_name_to_md5sum)


def add_merge_md5(in_mat, out_mat, assembly):
    """Conversion from source md5sum to merge md5sum when applicable."""
    datapath = "/nfs3_ib/ip29/ip29/jacques_group/local_ihec_data"

    md5_to_merge_md5 = merge_md5_dict(assembly, "2018-10", datapath)

    convert_matrix(in_mat, out_mat, md5_to_merge_md5)


def convert_md5sum(in_mat, out_mat):
    """Conversion from "md5sum" to "file_name"."""
    metadata = "/project/6007017/rabyj/epigeec/paper_annotate/hg19_2018-10_final_modif.json"

    md5sum_to_file_name = md5sum_to_file_name_dict(
        datasets=get_datasets_content(metadata)
    )

    convert_matrix(in_mat, out_mat, md5sum_to_file_name)


def main(argv):
    args = parse_args(argv)
    in_mat = args.matrix
    out_mat = args.output

    assembly = "hg19"

    # The content of these functions needs to be modified (paths)

    # convert_file_name(in_mat, out_mat, assembly)

    # add_merge_md5(in_mat, out_mat, assembly)

    convert_md5sum(in_mat, out_mat)


def cli():
    main(sys.argv[1:])

if __name__ == "__main__":
    cli()
