"""
Utility fonctions to examine dataset information (and more) of different signal groups.
For example, missing signals, or signals with specific errors.
"""
from __future__ import absolute_import, division, print_function

import argparse
from collections import Counter
import csv
import itertools
import os.path
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from epigeec_ihecdata import ihecdatasource

GENERAL_CATEGORIES = ["track_type", "status", "assay", "cell_type", "cell_type_category", "releasing_group", "dataset_name"]
DIFF_CATEGORIES = ["track_type", "assay", "cell_type", "cell_type_category", "releasing_group", "dataset_name"]
COUNTER_CATEGORIES = ["releasing_group", "track_type", "biomaterial_type", "assay", "status"]

DATASET_NAME = "dataset_name"
MD5SUM = "md5sum"
ASSAY = "assay"

STATUS = "status"
FINAL = "final"
INTER = "inter"

TRACK_TYPE_LABEL = "track_type"
SIGNAL_MERGED = "signal_merged"


def parse_args(args):
    """Define and return argument parser."""
    parser = argparse.ArgumentParser()
    parser.add_argument("assembly", help="Genome assembly.", choices=["hg19", "hg38", "mm10"])
    parser.add_argument("release", help="IHEC data portal build/release.", choices=["2016-11", "2017-10", "2018-10", "2019-11"])
    parser.add_argument("datapath", help="Path where the local IHEC data is located.")
    return parser.parse_args(args)

def get_md5_to_dset(local_src):
    """Return a md5sum:dset dict (info object) from metadata."""
    return {dset[MD5SUM]:dset for dset in local_src.read_flat_json()}

def get_md5_to_dset_name(local_src):
    """Return a md5sum:dataset_name dict from metadata."""
    return {dset[MD5SUM]:dset[DATASET_NAME] for dset in local_src.read_flat_json()}

def print_md5s_info(info):
    """Print dataset info from md5:dset object."""
    for md5, dset in info.items():
        print(md5)
        _print_general_info(dset)

def count_labels(info):
    """Print label counting info."""
    counters = _generate_counters()

    for dset in info.values():
        counters = _add_to_counters(dset, counters)

    for category, counter in counters.items():
        print("{}:\n{}\n".format(category, counter.most_common()))

def _get_md5s_info(md5s, local_src):
    """Return md5s general categories info as md5:{category:val} dict."""
    dsets = get_md5_to_dset(local_src)

    # keep only target dsets
    dsets = {
        md5:dset for md5, dset in dsets.items()
        if md5 in set(md5s)
        }

    print("{} md5s not in flat json".format(len(md5s) - len(dsets)))

    # keep only target categories
    for md5, dset in dsets.items():
        dsets[md5] = {
            category:val for category, val in dset.items()
            if category in set(GENERAL_CATEGORIES)
            }

    return dsets

def _print_general_info(dset):
    """Print general dataset info."""
    for label in GENERAL_CATEGORIES:
        print(dset[label])
    print('\n')

def _add_to_counters(dset, counters):
    """Add labels to categories counters."""
    for category in COUNTER_CATEGORIES:
        label = dset[category]
        counters[category][label] += 1
    return counters

def _generate_counters():
    """Return metadata_category:Counter() dict."""
    return {label:Counter() for label in COUNTER_CATEGORIES}

def missing_info(local_src):
    """Return dataset info of missing files."""
    md5s = set(local_src.read_missing_md5s())
    print("{} missing files information:\n".format(len(md5s)))
    return _get_md5s_info(md5s, local_src)

def chrom_err_info(local_src):
    """Return dataset info of files which produced a chromosome size error."""
    md5s = set(local_src.read_chromsize_error_md5s())
    print("{} bad chromsize files information:\n".format(len(md5s)))
    return _get_md5s_info(md5s, local_src)

def read_custom_md5_file(md5_file_path):
    """Return set of md5 present in md5 file."""
    return set([line.strip() for line in open(md5_file_path, 'r')])

def end_coordinate_error_info(md5_file_path, local_src):
    """Return dataset info of files which produced an end coordinate error."""
    md5s = read_custom_md5_file(md5_file_path)
    print("{} end coordinate error merge information:\n".format(len(md5s)))
    return _get_md5s_info(md5s, local_src)

def get_source_dset_and_md5s(md5_file_path, local_src):
    """Return list of [dataset_name, source md5 1, source md5 2...,]
    from a md5 file. Md5 file can contain merge or source md5s."""
    md5s = read_custom_md5_file(md5_file_path)

    md5_to_dset = get_md5_to_dset_name(local_src)
    md5_to_dset.update({
        merge_md5:dset_name for dset_name, merge_md5
        in local_src.read_expected_merges()
        })

    dset_md5s = _return_dset_md5s(local_src)

    source_dset = []
    for md5 in md5s:
        dset_name = md5_to_dset[md5]
        source_md5s = dset_md5s[dset_name]
        source_dset.append([dset_name] + source_md5s)

    return source_dset

def print_source_dset_and_md5s(md5_file_path, local_src):
    """Print in line dataset_name and used source md5s, from a md5 file.
    Md5 file can contain merge or source md5s.
    """
    for elem in get_source_dset_and_md5s(md5_file_path, local_src):
        print('\t'.join(elem))

def _find_merge_dset(target_md5s, merge_info):
    """Return set of datasets associated with target source IHEC md5s."""
    md5s_to_dset = {
        md5:dset
        for dset, merge_md5s in merge_info.items()
        for md5 in merge_md5s
        }
    return set(md5s_to_dset[md5] for md5 in target_md5s)

def verif_chrom_err_in_final(local_src):
    """Verify if expected results of merge which produced chrom error exist."""
    print("Are chromsize error merge present in the final signals?")

    # Get md5sums of one of the signal involved in failed merge with chromsize error
    source_md5s = set(local_src.read_chromsize_error_md5s())

    # Get linked dsets
    error_dsets = _find_merge_dset(
        target_md5s=source_md5s,
        merge_info=local_src.read_merge_info()
        )

    # Get the expected name (md5) of a successful merge
    error_signals = {
        dset:md5
        for dset, md5 in local_src.read_expected_merges()
        if dset in error_dsets
        }

    if len(source_md5s) != len(error_signals):
        message = "{} problematic source md5s/merge, {} found new names. Problem..."
        raise AssertionError(message.format(len(source_md5s), len(error_signals)))

    final_md5s = local_src.update_valid_standardize()
    big_problem = False
    for dset, md5 in error_signals.items():
        if md5 in final_md5s:
            big_problem = True
            print("Problem: {}\t{}".format(dset, md5))

    if not big_problem:
        print("No.")

def print_fraction_standardized(local_src):
    """Print fraction of standardized datasets."""
    nb_dsets = len(
        set(
            dset[DATASET_NAME]
            for dset in local_src.read_flat_json()
            if dset[STATUS] == INTER or dset[STATUS] == FINAL
        )
    )
    nb_standardized = len(local_src.update_valid_standardize())

    print("{} standardized signals/{} dsets. {:.2f}%".format(
        nb_standardized, nb_dsets, nb_standardized/nb_dsets*100))

def verify_dset_change(releases, assembly, datapath):
    """Return set of dataset names for datasets which vary in content
    between releases, for a given assembly

    A dataset_name:[md5sums] dict read from the flat json is used for this purpose.

    Only source IHEC signals are checked ("track_type" != "signal_merged").
    """
    datasources = [
        ihecdatasource.LocalIhecDatasource(assembly, release, datapath)
        for release in releases
        ]

    # Produce dset_name:[ihec_md5s] dict for each datasource
    dset_md5s_dicts = [_return_dset_md5s(local_src) for local_src in datasources]

    # For each unique unordered pair of releases, find different dsets
    problem_dset = []
    for i, j in itertools.combinations(range(len(releases)), 2):
        # print("----- {} VS {} -----\n".format(releases[i], releases[j]))
        dset_md5s_1 = dset_md5s_dicts[i]
        dset_md5s_2 = dset_md5s_dicts[j]
        common_dsets = set(dset_md5s_1.keys()) & set(dset_md5s_2.keys())
        for dset_name in common_dsets:
            md5s_1 = set(dset_md5s_1[dset_name])
            md5s_2 = set(dset_md5s_2[dset_name])
            if md5s_1 != md5s_2:
                problem_dset.append(dset_name)
                # print("{}\n{}\n{}\n".format(dset_name, md5s_1, md5s_2))

    return set(problem_dset)

def _return_dset_md5s(local_src):
    """Return non-merged dataset_name:[md5s] dict for assembly/release flat metadata."""
    metadata = local_src.read_flat_json()
    pairs = set((dset[DATASET_NAME], dset[MD5SUM]) for dset in metadata
                if dset[TRACK_TYPE_LABEL] != SIGNAL_MERGED)

    dset_md5s = {dset_name:[] for dset_name, _ in pairs}
    for dset_name, md5 in pairs:
        dset_md5s[dset_name].append(md5)

    return dset_md5s

def get_merge_md5_dict(local_src):
    """Return source_md5:merge_md5 dict."""
    merge_md5s = {dset:merge_md5 for dset, merge_md5 in local_src.read_expected_merges()}

    return {
        source_md5:merge_md5s[dset_name]
        for dset_name, source_md5s in local_src.read_merge_info().items()
        for source_md5 in source_md5s
        }

def print_epigeec_paper_annotate_md5_list(local_src):
    """Print list of hdf5 with assays used in the epiGeEC paper."""

    if local_src.assembly != "hg19":
        raise ValueError("Assembly is not hg19")

    paper_assays = set(["h3k4me1", "h3k4me3", "h3k27me3", "h3k36me3", "h3k9me3", "ctcf"])

    template = "/nfs3_ib/ip29/ip29/jacques_group/local_ihec_data/2019-11/hg19/hdf5/1kb_all_blklst/{md5}_1kb_all_blklst_value.hdf5"

    # some dsets have same md5
    md5s = set(
        dset[MD5SUM]
        for dset in local_src.read_final_json()
        if dset[ASSAY] in paper_assays
        )

    path_list = sorted([template.format(md5=md5) for md5 in md5s])

    for path in path_list:
        print(path)

def add_dset_info_to_diff_file(in_path, out_path, info):
    """Write to out_path compare_matrix difference file augmented with
    dataset info associated with md5s.
    """

    with open(in_path, 'r') as in_tsv, open(out_path, 'w') as out_tsv:
        reader = csv.reader(in_tsv, delimiter='\t')
        writer = csv.writer(out_tsv, delimiter='\t')

        header = next(reader)
        new_labels = [
            ["{}_1".format(label), "{}_2".format(label)]
            for label in DIFF_CATEGORIES
            ]
        header.extend(itertools.chain.from_iterable(new_labels))
        writer.writerow(header)

        for line in reader:
            md5_1, md5_2 = line[0:2]

            new_values = [
                [info[md5_1][label], info[md5_2][label]]
                for label in DIFF_CATEGORIES
                ]
            line.extend(itertools.chain.from_iterable(new_values))
            writer.writerow(line)

def add_dset_info_to_occurence_file(in_path, out_path, info):
    """Write to out_path compare_matrix difference file augmented with
    dataset info associated with md5s.
    """
    with open(in_path, 'r') as infile, open(out_path, 'w') as outfile:
        reader = csv.reader(infile, skipinitialspace=True, delimiter=' ')
        writer = csv.writer(outfile, delimiter='\t')

        header = ["occurence", "md5sum"] + DIFF_CATEGORIES
        writer.writerow(header)

        for line in reader:
            md5 = line[1]

            new_values = [
                info[md5][label]
                for label in DIFF_CATEGORIES
                ]
            line.extend(new_values)
            writer.writerow(line)

def get_all_expected_final_md5s(local_src):
    """Return the set of all expected md5s at the end of the
    standardization process. Includes source and merged files.
    """
    source_md5s_to_standardize = set(
        dset[MD5SUM] for dset in local_src.read_flat_json()
        if (dset[STATUS] == FINAL and dset[TRACK_TYPE_LABEL] != SIGNAL_MERGED)
    )

    expected_merge_md5s = set(md5 for _, md5 in local_src.read_expected_merges())

    return source_md5s_to_standardize | expected_merge_md5s

def get_null_md5_datasets(flat_json):
    """Return datasets where the md5s are null."""
    return set(dset["dataset_name"] for dset in flat_json
               if dset["md5sum"] is None)

def compare_merging_numbers(local_src):
    """Print the number of number of merges done with
    the new method compared to the old method (only one pair of f/r).
    """
    final_dsets = set(dset[DATASET_NAME] for dset in local_src.read_final_json())
    merge_info = local_src.read_merge_info()
    md5_to_dset = get_md5_to_dset(local_src)
    old_merge_signal_types = set(["signal_forward", "signal_reverse"])

    expected_old_merge = 0
    done_old_merge = 0
    done_new_merge = 0
    for dset_name, md5s in merge_info.items():

        done = False
        if dset_name in final_dsets:
            done = True
            done_new_merge += 1

        if len(md5s) == 2 and md5_to_dset[md5s[0]][TRACK_TYPE_LABEL] in old_merge_signal_types:
            expected_old_merge += 1
            if done:
                done_old_merge += 1

    print("Expected merge with old method: {} (done: {})".format(expected_old_merge, done_old_merge))
    print("Expected merge with new method: {} (done: {})".format(len(merge_info), done_new_merge))


def main(argv):
    """Run one (or more) of many utility fonctions to gather dataset information."""
    args = parse_args(argv)
    assembly = args.assembly
    release = args.release
    datapath = args.datapath

    # local_src = ihecdatasource.LocalIhecDatasource(assembly, release, datapath)

    # compare_merging_numbers(local_src)

    # md5_file = "/project/6007017/rabyj/galaxy_matrix_all_2017-10/converted/{}_2017-10_merge_md5.md5".format(assembly)
    # md5s_2017 = set(read_custom_md5_file(md5_file))
    # md5s_local_src = set(dset[MD5SUM] for dset in local_src.read_final_json() if dset[TRACK_TYPE_LABEL] == SIGNAL_MERGED)

    # for md5 in sorted(md5s_2017 & md5s_local_src):
    #     print(md5)

    # -- test for presence of specific 2018 source files in 2019 --

    # local_2018_src = ihecdatasource.LocalIhecDatasource(assembly, "2018-10", datapath)
    # local_2019_src = ihecdatasource.LocalIhecDatasource(assembly, "2019-11", datapath)

    # all_2019_md5s = set(dset[MD5SUM] for dset in local_2019_src.read_flat_json())

    # md5_filepath = "/project/6007017/rabyj/epigeec/mergetools/standardize_bigwig/script/hg38_2018-10/from_merge_run1/merge_end_coordinate_error.md5"
    # merge_errors_2018 = get_source_dset_and_md5s(md5_filepath, local_2018_src)
    # merge_error_source_md5s = set(
    #     md5
    #     for elem in merge_errors_2018
    #     for md5 in elem[1:]
    # )
    # print(len(merge_error_source_md5s), len(all_2019_md5s))
    # print(len(merge_error_source_md5s & all_2019_md5s))

    # --- Info object printing ---

    # info = chrom_err_info(local_src)
    # info = missing_info(local_src)

    # path = "/home/rabyj/rabyj/epigeec/mergetools/standardize_bigwig/script/hg38_2018-10/from_merge_run1/end_coordinate_error.md5"
    # info = end_coordinate_error_info(path, local_src)

    # print_md5s_info(info)
    # count_labels(info)

    # path = "/home/rabyj/rabyj/epigeec/mergetools/merge_bigwig/script/mm10_2019-11/bbiOutputOneSummaryFurtherReduce_error.md5"
    # path = "/nfs3_ib/ip29/ip29/jacques_group/local_ihec_data/2018-10/hg38/hg38_2018-10_missing.md5"
    # print_source_dset_and_md5s(path, local_src)

    # --- Other ---

    # verif_chrom_err_in_final(local_src)

    # print_fraction_standardized(local_src)

    # problem_dset = verify_dset_change(["2016-11", "2017-10", "2018-10"], assembly, datapath)
    # for dset_name in sorted(problem_dset):
    #     print(dset_name)

    # print_epigeec_paper_annotate_md5_list(local_src)

    # --- Create augmented files ---

    # base = "/project/6007017/rabyj/epigeec/pipeline_final_checks/compare_matrix/VS_2018-10/"
    # info = get_md5_to_dset(local_src)

    # in_diff = base + "hg38_2017-10_merge_md5_n10k.tsv"
    # out_diff = base + "hg38_2017-10_merge_md5_n10k_plus.tsv"
    # add_dset_info_to_diff_file(in_diff, out_diff, info)

    # in_path = base + "n10k_most_occurence/hg38_2017-10_merge_md5_n10k.md5"
    # out_path = base + "n10k_most_occurence/hg38_2017-10_merge_md5_n10k_occurence_plus.tsv"
    # add_dset_info_to_occurence_file(in_path, out_path, info)

    # --- test whether every md5 involved in first metadata pass of 2019-11 is also involved in merge with final IHEC portal metadata ---

    # local_src = ihecdatasource.LocalIhecDatasource(assembly, "2019-11", datapath)
    # to_merge_2019_new = set(itertools.chain.from_iterable(local_src.read_merge_info().values()))

    # to_merge_2019_old = set(itertools.chain.from_iterable(
    #     ihecdatasource.LocalIhecDatasourceReader.to_merge(
    #         os.path.join(local_src.local_dir, "old_metadata/{}_2019-11_merge.tsv".format(local_src.assembly))
    #         ).values()
    #     ))
    # print(len(to_merge_2019_old), len(to_merge_2019_new))
    # print(to_merge_2019_old.issubset(to_merge_2019_new))


def cli():
    main(sys.argv[1:])

if __name__ == "__main__":
    cli()
