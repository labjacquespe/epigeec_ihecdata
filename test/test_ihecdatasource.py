from __future__ import print_function
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from epigeec_ihecdata import ihecdatasource

DATAPATH = "/nfs3_ib/ip29/ip29/jacques_group/local_ihec_data"

def find_merge(assembly, release):
    mount_src = ihecdatasource.Mp2bMountIhecDatasource(assembly, release)
    merges = []
    for k, v in sorted(mount_src._paths.items()):
        if k.split('.')[-1] == "merge":
            merges.append([k, v])
    return merges


def test_Mp2bMountIhecDatasource():

    # -- test init/ _find_files --
    mount = ihecdatasource.Mp2bMountIhecDatasource("hg19", "2018-10")

    # print(mount.assembly, mount.release, mount.DIR, mount.SPECIAL_HG38_SHIRAHIGE)

    for k, v in sorted(mount._paths.items()):
        # if len(k.split('.')[0]) != 32:
        if len(k) != 32:
            print(k, v)

    #pour hg19, 2018-10: capture md5 + md5.merge + md5.merge.bedGraph + report.txt (lone file from kanai)
    #pour hg38, 2018-10 capture les trois avec md5 + json/log de shirahige (6 fichiers)

    # -- test get_ihecdatas --
    # ihecdatas = mount.get_ihecdatas(mount._paths.keys())
    # for ihecdata in ihecdatas:
    #     print(ihecdata.basename)


def test_LocalIhecDatasource():
    src = ihecdatasource.LocalIhecDatasource("hg19", "2018-10", DATAPATH)
    print(src.current_mergename_md5s)

def test_Hdf5Dir():
    local_src = ihecdatasource.LocalIhecDatasource("hg19", "2018-10", DATAPATH)

    local_hdf5_dir = ihecdatasource.Hdf5Dir(local_src.hdf5_dir)
    local_hdf5_dir.print_hdf5_info()

    # local_src.find_missing_hdf5s(verbose=True)

def main():

    # test_Mp2bMountIhecDatasource()
    # test_LocalIhecDatasource()

    # test_Hdf5Dir()

    for assembly in ["hg19", "hg38", "mm10"]:
        for release in ["2016-11", "2017-10"]:
            merges = find_merge(assembly, release)
            filename = "./{}_{}_merge.list".format(assembly, release)
            with open(filename, 'w') as output:
                for basename, path in merges:
                    output.write("{}\t{}\n".format(basename.split('.')[0], path))


if __name__ == "__main__":
    main()
